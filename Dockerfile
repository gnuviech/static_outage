FROM node:10

RUN npm install inliner

ENTRYPOINT /node_modules/.bin/inliner

ADD *.png /
ADD *.css /
