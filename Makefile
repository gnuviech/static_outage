IMAGES=$(wildcard *.jpg *.png)
CSSFILES=$(wildcard *.css)

index.min.html: index.html $(CSSFILES) $(IMAGES)
	docker build -t inliner:latest .
	docker run -i --rm inliner:latest <"$<" >"$@"

clean:
	rm -f index.min.html
